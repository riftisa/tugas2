package com.example.landingpage_login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity1 extends AppCompatActivity {
    public EditText userEdt, passEdt;
    private Button loginBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();
        setVariable();
    }

    private void setVariable(){
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(userEdt.getText().toString().isEmpty() && passEdt.getText().toString().isEmpty()){
                   Toast.makeText(LoginActivity1.this, "Please fill the fom of login", Toast.LENGTH_SHORT).show();
               }else{
                   String userStr = userEdt.getText().toString();
                   String passStr = passEdt.getText().toString();

                   Intent intent = new Intent(LoginActivity1.this, MainActivity.class);
                   intent.putExtra("username",userStr);
                   intent.putExtra("password",passStr);
                   startActivity(intent);
//                   startActivity(new Intent(LoginActivity1.this, MainActivity.class));
               }
            }
        });
    }
    private void initView(){
        userEdt=findViewById(R.id.editTextText2);
        passEdt=findViewById(R.id.editTextText2Nim);
        loginBtn=findViewById(R.id.LoginButton);
    }

}